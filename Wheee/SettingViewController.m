//
//  SettingViewController.m
//  Wheee
//
//  Created by aisoter on 2016/4/20.
//  Copyright © 2016年 Hungchi. All rights reserved.
//

#import "SettingViewController.h"
#import "AccountViewController.h"

@interface SettingViewController ()

@property NSMutableArray *informationArray;
@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray *supportArray = [[NSArray alloc] initWithObjects:@"意見回饋", @"使用說明", nil];
    NSArray *aboutArray = [[NSArray alloc] initWithObjects:@"隱私政策", @"臉書粉絲專頁", @"網站", nil];
    NSArray *accountArray = [[NSArray alloc] initWithObjects:@"帳號登出", nil];
    
    self.informationArray = [[NSMutableArray alloc] initWithObjects:supportArray, aboutArray,accountArray, nil];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.informationArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.informationArray objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text = [[self.informationArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    if (indexPath.section == 2) {
        [cell.textLabel setTextColor:[UIColor redColor]];
        UIFont *myFont = [UIFont fontWithName: @"PingFang TC" size: 14.0 ];
        cell.textLabel.font  = myFont;
    }
    else{
        [cell.textLabel setTextColor:[UIColor colorWithRed:0.137 green:0.243 blue:0.4 alpha:1.0]];
        UIFont *myFont = [UIFont fontWithName: @"PingFang TC" size: 14.0 ];
        cell.textLabel.font  = myFont;
    }
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return @"支援";
            break;
        case 1:
            return @"關於";
            break;
        case 2:
            return @"帳號";
            break;
        default:
            return @"";
            break;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.contentView.backgroundColor = [UIColor colorWithRed:0.953 green:0.953 blue:0.953 alpha:1.0];
    [header.textLabel setTextColor:[UIColor colorWithRed:0.608 green:0.608 blue:0.608 alpha:1.0]];
    UIFont *myFont = [UIFont fontWithName: @"PingFang TC" size: 12.0 ];
    header.textLabel.font  = myFont;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

@end
