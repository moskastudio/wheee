//
//  ProductTableViewCell.h
//  Wheee
//
//  Created by Hungchi on 2/20/16.
//  Copyright © 2016 Hungchi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (strong, nonatomic) IBOutlet UILabel *productName;
@property (strong, nonatomic) IBOutlet UILabel *productSchool;
@property (strong, nonatomic) IBOutlet UILabel *likeCount;
@property (strong, nonatomic) IBOutlet UILabel *recordCount;
@property (strong, nonatomic) IBOutlet UIImageView *promoteImage;


@end
