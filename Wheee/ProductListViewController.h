//
//  ProductListViewController.h
//  Wheee
//
//  Created by Hungchi on 1/29/16.
//  Copyright © 2016 Hungchi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductListViewController : UIViewController
<UITableViewDataSource, UITableViewDelegate>

@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSString *imageFile;

@end
