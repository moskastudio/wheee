//
//  PicgoService.m
//  Picgo
//
//  Created by Hungchi on 7/15/15.
//  Copyright (c) 2015 Hungchi. All rights reserved.
//

#import "PicgoService.h"
#import "JSONKit.h"
#import "AFHTTPSessionManager.h"

NSString* InsertUserSuccessNotification = @"InsertUserSuccessNotification";
NSString* InsertPhotoSuccessNotification = @"InsertPhotoSuccessNotification";
NSString* InsertAudioSuccessNotification = @"InsertAudioSuccessNotification";
NSString* DeleteAudioSuccessNotification = @"DeleteAudioSuccessNotification";
NSString* DeletePhotoSuccessNotification = @"DeletePhotoSuccessNotification";
NSString* SendImageSuccessNotification = @"SendImageSuccessNotification";
NSString* SendAudioSuccessNotification = @"SendAudioSuccessNotification";

NSString* FetchPhotosByNameSuccessNotification = @"FetchPhotosByNameSuccessNotification";
NSString* FetchAllAudiosByPhotoIDSuccessNotification = @"FetchAllAudiosByPhotoIDSuccessNotification";
NSString* FetchAudioWithTagsByPhotoIDSuccessNotification = @"FetchAudioWithTagsByPhotoIDSuccessNotification";
NSString* FetchTranscriptionSuccessNotification = @"FetchTranscriptionSuccessNotification";
NSString* FetchStoryByPhotoNameSuccessNotification = @"FetchStoryByPhotoNameSuccessNotification";
NSString* FetchCKIPResultSuccessNotification = @"FetchCKIPResultSuccessNotification";
NSString* FetchTagsByAudioNameSuccessNotification = @"FetchTagsByAudioNameSuccessNotification";
NSString* FetchPhotosByCosSimilaritySuccessNotification = @"FetchPhotosByCosSimilaritySuccessNotification";


static NSString *serverURL = @"http://hungyaki.com/PicMemory/API/";

@implementation PicgoService

+ (id) sharedInstance
{
    static PicgoService* sharedMyInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyInstance = [[self alloc] initPicgoService];
    });
    return sharedMyInstance;
}

+ (UIImage *)imageWithImage:(UIImage *)image {
    
    CGSize newSize = CGSizeMake(image.size.width/10, image.size.height/10);
    NSLog(@"newSize:%@", NSStringFromCGSize(newSize));
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (id) initPicgoService
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

// Insert
- (void)insertUser:(NSString*) userName {
    NSString *URLString = [NSString stringWithFormat:@"%@insertUser.php", serverURL];
    NSLog(@"URLString:%@", URLString);
    NSDictionary *parameters = @{@"userName": userName};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager GET:URLString parameters:parameters progress:nil success:^(NSURLSessionTask *operation, id responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        NSLog(@"respnseStr:%@", responseStr);
        if ([[jsonDict objectForKey:@"status"] isEqualToString:@"success"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:InsertUserSuccessNotification object:[jsonDict objectForKey:@"result"]];
        }
        else {
            // handle errors
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)insertPhoto:(NSString*) userID andUserName:(NSString*) userName andPhotoName:(NSString*) photoName {
    NSString *URLString = [NSString stringWithFormat:@"%@insertPhoto.php", serverURL];
    NSDictionary *parameters = @{@"userID": userID, @"userName": userName, @"photoName": photoName};
    NSLog(@"parameters:%@", parameters);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:URLString parameters:parameters progress:nil success:^(NSURLSessionTask *operation, id responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        if ([[jsonDict objectForKey:@"status"] isEqualToString:@"success"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:InsertPhotoSuccessNotification object:[jsonDict objectForKey:@"result"]];
        }
        else {
            // handle errors
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)insertAudio:(NSString*) photoID andPhotoName:(NSString*) photoName andAudioName:(NSString*) audioName andLength:(NSString*) length andPosition:(NSString*) position {
    NSString *URLString = [NSString stringWithFormat:@"%@insertAudio.php", serverURL];
    NSDictionary *parameters = @{@"photoID": photoID, @"photoName": photoName, @"audioName": audioName, @"length": length, @"position": position};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:URLString parameters:parameters progress:nil success:^(NSURLSessionTask *operation, id responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        //NSLog(@"insertAudio - responseStr:%@", responseStr);
        
        if ([[jsonDict objectForKey:@"status"] isEqualToString:@"success"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:InsertAudioSuccessNotification object:[jsonDict objectForKey:@"result"]];
        }
        else {
            // handle errors
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)deleteAudio:(NSString*) audioName{
    NSString *URLString = [NSString stringWithFormat:@"%@deleteAudio.php", serverURL];
    NSDictionary *parameters = @{@"audioName": audioName};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:URLString parameters:parameters progress:nil success:^(NSURLSessionTask *operation, id responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        //NSLog(@"deleteAudio - responseStr:%@", responseStr);
        
        if ([[jsonDict objectForKey:@"status"] isEqualToString:@"success"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:DeleteAudioSuccessNotification object:[jsonDict objectForKey:@"result"]];
        }
        else {
            // handle errors
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)deletePhoto:(NSString*) photoName{
    NSString *URLString = [NSString stringWithFormat:@"%@deletePhoto.php", serverURL];
    NSDictionary *parameters = @{@"photoName": photoName};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:URLString parameters:parameters progress:nil success:^(NSURLSessionTask *operation, id responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        //NSLog(@"deletePhoto - responseStr:%@", responseStr);
        
        if ([[jsonDict objectForKey:@"status"] isEqualToString:@"success"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:DeletePhotoSuccessNotification object:[jsonDict objectForKey:@"result"]];
        }
        else {
            // handle errors
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)sendImage:(UIImage*) file andFileName:(NSString*) filename{
    NSString *URLString = [NSString stringWithFormat:@"%@apiSendImage.php", serverURL];
    //NSData* imageData = UIImagePNGRepresentation([PicgoService imageWithImage:file]);
    NSData* imageData = UIImageJPEGRepresentation(file, 1.0);
    
    NSUInteger imageSize = imageData.length;
    NSLog(@"SIZE OF IMAGE: %ld ", (unsigned long)imageSize);
    
    AFHTTPSessionManager* manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:URLString parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:@"file" fileName:filename mimeType:@"image/jpeg"];
    }  progress:nil success:^(NSURLSessionTask* operation, id responseObject) {

        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        NSLog(@"sendImage - success: %@", responseStr);

        
        if ([[jsonDict objectForKey:@"status"] isEqualToString:@"success"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:SendImageSuccessNotification object:jsonDict];
        }
        else {
            // handle errors
            NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            NSLog(@"sendImage - fail: %@", responseStr);
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:SendImageSuccessNotification object:[jsonDict objectForKey:@"result"]];
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)sendAudio:(NSData*) file andFileName:(NSString*) filename{
    NSString *URLString = [NSString stringWithFormat:@"%@apiSendAudio.php", serverURL];
    
    AFHTTPSessionManager* manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:URLString parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:file name:@"file" fileName:filename mimeType:@"audio/m4a"];
        }  progress:nil success:^(NSURLSessionTask* operation, id responseObject) {
        
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        if ([[jsonDict objectForKey:@"status"] isEqualToString:@"success"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:SendAudioSuccessNotification object:[jsonDict objectForKey:@"result"]];
        }
        else {
            // handle errors
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

}

- (void)fetchPhotosByName:(NSString*) userName{
    NSString *URLString = [NSString stringWithFormat:@"%@apiGetPhotosByUserName.php", serverURL];
    NSLog(@"URLString:%@", URLString);
    NSDictionary *parameters = @{@"userName": userName};
    NSLog(@"parameters:%@", parameters);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager GET:URLString parameters:parameters progress:nil success:^(NSURLSessionTask *operation, id responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        if ([[jsonDict objectForKey:@"status"] isEqualToString:@"success"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:FetchPhotosByNameSuccessNotification object:[jsonDict objectForKey:@"result"]];
        }
        else {
            // handle errors
            //NSLog(@"responseStr:%@", responseStr);
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)fetchAllAudiosByPhotoID:(NSString*) photoID{
    NSString *URLString = [NSString stringWithFormat:@"%@apiGetAllAudiosByPhotoID.php", serverURL];
    NSDictionary *parameters = @{@"photoID": photoID};
    NSLog(@"fetchAllAudiosByPhotoID:%@", parameters);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:URLString parameters:parameters progress:nil success:^(NSURLSessionTask *operation, id responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        //NSLog(@"responseStr:%@", responseStr);
        if ([[jsonDict objectForKey:@"status"] isEqualToString:@"success"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:FetchAllAudiosByPhotoIDSuccessNotification object:[jsonDict objectForKey:@"result"]];
        }
        else {
            // handle errors
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)fetchAudioWithTagsByPhotoID:(NSString*) photoID {
    NSString *URLString = [NSString stringWithFormat:@"%@apiGetAudioWithTagsByPhotoID.php", serverURL];
    NSDictionary *parameters = @{@"photoID": photoID};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:URLString parameters:parameters progress:nil success:^(NSURLSessionTask *operation, id responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        if ([[jsonDict objectForKey:@"status"] isEqualToString:@"success"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:FetchAudioWithTagsByPhotoIDSuccessNotification object:[jsonDict objectForKey:@"result"]];
        }
        else {
            // handle errors
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)fetchTranscription:(NSString *) audioName {
    NSString *URLString = [NSString stringWithFormat:@"%@apiGetTranscript.php", serverURL];
    NSDictionary *parameters = @{@"audioName": audioName};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:URLString parameters:parameters progress:nil success:^(NSURLSessionTask *operation, id responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        if ([[jsonDict objectForKey:@"status"] isEqualToString:@"success"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:FetchTranscriptionSuccessNotification object:[jsonDict objectForKey:@"result"]];
        }
        else {
            // handle errors
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)fetchStoryByPhotoName:(NSString*) photoName {
    NSString *URLString = [NSString stringWithFormat:@"%@apiGetStoryByPhotoName.php", serverURL];
    NSDictionary *parameters = @{@"photoName": photoName};
    
    NSLog(@"parameters:%@", parameters);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:URLString parameters:parameters progress:nil success:^(NSURLSessionTask *operation, id responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        if ([[jsonDict objectForKey:@"status"] isEqualToString:@"success"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:FetchStoryByPhotoNameSuccessNotification object:jsonDict];
        }
        else {
            // handle errors
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)fetchCKIPResult:(NSString*) text {
    NSLog(@"fetchCKIPResult:%@", text);
    NSString *URLString = [NSString stringWithFormat:@"%@apiGetCKIPResult.php", serverURL];
    NSDictionary *parameters = @{@"text": text};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:URLString parameters:parameters progress:nil success:^(NSURLSessionTask *operation, id responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        if ([[jsonDict objectForKey:@"status"] isEqualToString:@"success"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:FetchCKIPResultSuccessNotification object:jsonDict];
        }
        else {
            // handle errors
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)fetchTagsByAudioName:(NSString*) audioName {
    NSString *URLString = [NSString stringWithFormat:@"%@apiGetTagsByAudioName.php", serverURL];
    NSDictionary *parameters = @{@"audioName": audioName};
    NSLog(@"fetchTagsByAudioName:%@", parameters);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:URLString parameters:parameters progress:nil success:^(NSURLSessionTask *operation, id responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        if ([[jsonDict objectForKey:@"status"] isEqualToString:@"success"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:FetchTagsByAudioNameSuccessNotification object:jsonDict];
        }
        else {
            // handle errors
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)fetchPhotosByCosSimilarity:(NSString*) userName andTags:(NSString*) tags{
    NSString *URLString = [NSString stringWithFormat:@"%@apiGetPhotosByCosSimilarity.php", serverURL];
    NSDictionary *parameters = @{@"userName": userName, @"tags" : tags};
    NSLog(@"fetchPhotosByCosSimilarity:%@", parameters);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:URLString parameters:parameters progress:nil success:^(NSURLSessionTask *operation, id responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        if ([[jsonDict objectForKey:@"status"] isEqualToString:@"success"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:FetchPhotosByCosSimilaritySuccessNotification object:jsonDict];
        }
        else {
            // handle errors
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
}

@end
