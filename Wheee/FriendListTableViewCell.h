//
//  FriendListTableViewCell.h
//  Wheee
//
//  Created by aisoter on 2016/4/20.
//  Copyright © 2016年 Hungchi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendListTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *profileImage;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *dynamicState;
@property (strong, nonatomic) IBOutlet UIImageView *icnLike;
@property (strong, nonatomic) IBOutlet UILabel *likeCount;
@property (strong, nonatomic) IBOutlet UIImageView *icnNote;
@property (strong, nonatomic) IBOutlet UILabel *noteCount;

@end
