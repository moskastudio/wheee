//
//  PicgoService.h
//  Picgo
//
//  Created by Hungchi on 7/15/15.
//  Copyright (c) 2015 Hungchi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

extern NSString* InsertUserSuccessNotification;
extern NSString* InsertPhotoSuccessNotification;
extern NSString* InsertAudioSuccessNotification;
extern NSString* DeleteAudioSuccessNotification;
extern NSString* DeletePhotoSuccessNotification;
extern NSString* SendImageSuccessNotification;
extern NSString* SendAudioSuccessNotification;

extern NSString* FetchPhotosByNameSuccessNotification;
extern NSString* FetchAllAudiosByPhotoIDSuccessNotification;
extern NSString* FetchAudioWithTagsByPhotoIDSuccessNotification;
extern NSString* FetchTranscriptionSuccessNotification;
extern NSString* FetchStoryByPhotoNameSuccessNotification;
extern NSString* FetchCKIPResultSuccessNotification;
extern NSString* FetchTagsByAudioNameSuccessNotification;
extern NSString* FetchPhotosByCosSimilaritySuccessNotification;

@interface PicgoService : NSObject

+ (id) sharedInstance;

// Insert
- (void)insertUser:(NSString*) userName;
- (void)insertPhoto:(NSString*) userID andUserName:(NSString*) userName andPhotoName:(NSString*) photoName;
- (void)insertAudio:(NSString*) photoID andPhotoName:(NSString*) photoName andAudioName:(NSString*) audioName andLength:(NSString*) length andPosition:(NSString*) position;
- (void)deleteAudio:(NSString*) audioName;
- (void)deletePhoto:(NSString*) photoName;

- (void)sendImage:(UIImage*) file andFileName:(NSString*) filename;
- (void)sendAudio:(NSData*) file andFileName:(NSString*) filename;

// Fetch
- (void)fetchPhotosByName:(NSString*) userName;
- (void)fetchAllAudiosByPhotoID:(NSString*) photoID;
- (void)fetchAudioWithTagsByPhotoID:(NSString*) photoID;
- (void)fetchTranscription:(NSString*) audioName;
- (void)fetchStoryByPhotoName:(NSString*) photoName;
- (void)fetchCKIPResult:(NSString*) text;
- (void)fetchTagsByAudioName:(NSString*) audioName;
- (void)fetchPhotosByCosSimilarity:(NSString*) userName andTags:(NSString*) tags;

@end
