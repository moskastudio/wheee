//
//  AccountViewController.m
//  Wheee
//
//  Created by aisoter on 2016/4/19.
//  Copyright © 2016年 Hungchi. All rights reserved.
//

#import "AccountViewController.h"
#import "FriendListTableViewCell.h"
#import "SettingViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "AppDelegate.h"
#import "FriendViewController.h"

@interface AccountViewController ()

@end

@implementation AccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _porfileImage.layer.cornerRadius = 42;
    _porfileImage.layer.masksToBounds = YES;
    _porfileImage.layer.borderWidth = 2;
    _porfileImage.layer.borderColor = [UIColor colorWithRed:0.137 green:0.243 blue:0.4 alpha:1.0].CGColor;
    
    _inviteFriendBackground.layer.cornerRadius = 5.0;
    _inviteFriendBackground.layer.masksToBounds = YES;
    
    _inviteFriendBtn.layer.cornerRadius = 5.0;
    _inviteFriendBtn.layer.masksToBounds = YES;
    _inviteFriendBtn.layer.borderWidth = 1;
    _inviteFriendBtn.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.FB_UserID = [FBSDKProfile currentProfile].userID;
    appDelegate.FB_UesrName = [FBSDKProfile currentProfile].name;
    NSLog(@"留言:::%@,%@",appDelegate.FB_UserID,appDelegate.FB_UesrName);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 75;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *friendsListIdentifier = @"friendsListCell";
    FriendListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:friendsListIdentifier];
    
    if (cell == nil) {
        cell = [[FriendListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:friendsListIdentifier];
    }
    cell.profileImage.layer.cornerRadius = 30;
    cell.profileImage.layer.masksToBounds = YES;
    cell.profileImage.layer.borderWidth = 1;
    cell.profileImage.layer.borderColor = [UIColor colorWithRed:0.137 green:0.243 blue:0.4 alpha:1.0].CGColor;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"didSelectRowAtIndexPath,indexPath.row:::%ld",indexPath.row);
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    self.friendsListTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self performSegueWithIdentifier:@"showFriendDetail" sender:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)inviteFriendBtn:(id)sender {
//    FBSDKAppInviteContent *content =[[FBSDKAppInviteContent alloc] init];
//    NSString *urlString = @"https://fb.me/827005577403290";
//    content.appLinkURL = [NSURL URLWithString:urlString];
//    [FBSDKAppInviteDialog showWithContent:content delegate:self];
}
- (IBAction)goSetting:(id)sender {
    [self performSegueWithIdentifier:@"goSetting" sender:self];
}
@end
