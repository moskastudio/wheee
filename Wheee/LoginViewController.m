//
//  LoginViewController.m
//  Wheee
//
//  Created by Hungchi on 1/29/16.
//  Copyright © 2016 Hungchi. All rights reserved.
//

#import "LoginViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "AppDelegate.h"

@interface LoginViewController ()
- (IBAction)skipAction:(id)sender;
@property NSTimer *scanTimer;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    
//    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
//    loginButton.readPermissions = @[@"public_profile", @"email", @"user_friends"];
//    loginButton.center = self.view.center;
//    [loginButton addTarget:self action:@selector(loginButtonClicked) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:loginButton];
    
    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
    loginButton.center = self.view.center;
    [self.view addSubview:loginButton];
    
    self.loginButton.readPermissions = @[@"public_profile", @"email", @"user_friends"];
    self.loginButton.delegate = self;
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(profileUpdated:) name:FBSDKProfileDidChangeNotification object:nil];
    
//    UIButton *myLoginButton=[UIButton buttonWithType:UIButtonTypeCustom];
//    UIImage *btnImage = [UIImage imageNamed:@"FB_Login.png"];
//    [myLoginButton setImage:btnImage forState:UIControlStateNormal];
//    myLoginButton.frame=CGRectMake(0,0,200,40);
//    myLoginButton.center = self.view.center;
//    
//    // Handle clicks on the button
//    [myLoginButton
//     addTarget:self
//     action:@selector(loginButtonClicked) forControlEvents:UIControlEventTouchUpInside];
//    
////     Add the button to the view
//    [self.view addSubview:myLoginButton];
    
    // Do any additional setup after loading the view.

}

-(void)profileUpdated:(NSNotification *) notification{
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    appDelegate.FB_UserID = [FBSDKProfile currentProfile].name;
    appDelegate.FB_UesrName = [FBSDKProfile currentProfile].userID;
    NSLog(@"User name: %@",[FBSDKProfile currentProfile].name);
    NSLog(@"User ID: %@",[FBSDKProfile currentProfile].userID);
    NSLog(@"User linkURL: %@",[FBSDKProfile currentProfile].linkURL);
    if ([FBSDKAccessToken currentAccessToken]) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 NSLog(@"fetched user:%@", result);
             }
         }];
    }
    [self performSegueWithIdentifier:@"showMainPage" sender:self];
}
- (void) loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result                error:(NSError *)error{
}

- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton{
}

-(void)loginButtonClicked{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             NSLog(@"Logged in");
             NSLog(@"_userID:::%@",_userID);
//             [self performSegueWithIdentifier:@"showMainPage" sender:self];
         }
     }];
    [login logOut];
}

-(void)viewDidAppear:(BOOL)animated{
//    [self performSegueWithIdentifier:@"showMainPage" sender:self];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    appDelegate.FB_UserID = [FBSDKProfile currentProfile].name;
    appDelegate.FB_UesrName = [FBSDKProfile currentProfile].userID;
    if (appDelegate.FB_UserID != NULL) {
        [self performSegueWithIdentifier:@"showMainPage" sender:self];
    }
    else{
        FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
        loginButton.center = self.view.center;
        [self.view addSubview:loginButton];
        
        self.loginButton.readPermissions = @[@"public_profile", @"email", @"user_friends"];
        self.loginButton.delegate = self;
        [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(profileUpdated:) name:FBSDKProfileDidChangeNotification object:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)skipAction:(id)sender {
    [self performSegueWithIdentifier:@"showMainPage" sender:self];
}
@end
