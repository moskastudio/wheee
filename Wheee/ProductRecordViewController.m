//
//  ProductRecordViewController.m
//  Wheee
//
//  Created by Hungchi on 3/14/16.
//  Copyright © 2016 Hungchi. All rights reserved.
//

#import "ProductRecordViewController.h"
#import "productRecordTableViewCell.h"

@interface ProductRecordViewController ()

@property NSMutableArray *productProfilePic;
@property NSMutableArray *productName;
@property NSMutableArray *productSchool;
@property NSMutableArray *productLikeCount;
@property NSMutableArray *productMessage;
@property NSMutableArray *productImage;
@property NSMutableArray *productImage2;
@property NSInteger cellTag;
@property NSArray *imageArray;

@end

@implementation ProductRecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _goEditButton.layer.borderWidth = 1.0;
    _goEditButton.layer.borderColor = [[UIColor colorWithRed:0.847 green:0.847 blue:0.847 alpha:1.0] CGColor];
    _goEditButton.layer.cornerRadius = 5.0;
    _goEditButton.clipsToBounds = YES;
    
    self.productProfilePic = [[NSMutableArray alloc] initWithObjects:@"sample.png",@"sample2.png",@"sample3.png", nil];
    self.productName = [[NSMutableArray alloc] initWithObjects:@"小明與小美",@"爆炸頭阿蟹",@"另一個宇宙", nil];
    self.productSchool = [[NSMutableArray alloc] initWithObjects:@"台灣科技大",@"雲科大",@"台科大", nil];
    self.productLikeCount = [[NSMutableArray alloc] initWithObjects:@"100",@"9999",@"50", nil];
    self.productMessage = [[NSMutableArray alloc] initWithObjects:@"wheee~我要生個基督徒小孩，然後買給她玩！另一個宇宙小明與小美爆炸頭阿蟹",@"wheee~wheee~wheee~wheee~wheee~wheee~wheee~wheee~",@"另一個宇宙小明與小美爆炸頭阿蟹", nil];
    
    NSArray *image1 = [[NSArray alloc]initWithObjects:@"test.jpg",@"test3.jpg", nil];
    NSArray *image2 = [[NSArray alloc]initWithObjects:@"test2.jpg",@"sample4.png", nil];
    NSArray *image3 = [[NSArray alloc]initWithObjects:@"sample6.png",@"", nil];

    self.imageArray = [NSArray arrayWithObjects:[UIImage imageNamed:@"test.jpg"],
                       [UIImage imageNamed:@"sample6.png"],
                       [UIImage imageNamed:@"sample.png"],
                       [UIImage imageNamed:@"sample4.png"],
                       [UIImage imageNamed:@"test2.jpg"],
                       nil];

    self.productImage = [[NSMutableArray alloc] init];
    [self.productImage addObjectsFromArray:image1];
    [self.productImage addObjectsFromArray:image2];
    [self.productImage addObjectsFromArray:image3];
    
//    self.productImage = [[NSMutableArray alloc] initWithObjects:@"sample6.png",@"sample7.png",@"sample.png", nil];
//    self.productImage2 = [[NSMutableArray alloc] initWithObjects:@"sample.png",@"",@"sample4.png", nil];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *productRecordIdentifier = @"recordCell";
    productRecordTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:productRecordIdentifier];
    return cell.bounds.size.height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.productName.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *productRecordIdentifier = @"recordCell";
    productRecordTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:productRecordIdentifier];
    
    for (UIView *view in cell.productImageView.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            [view removeFromSuperview];
        }
    }
    
    if (cell == nil) {
        cell = [[productRecordTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:productRecordIdentifier];
    }
    cell.tableCellBG.frame = CGRectMake(self.view.window.frame.size.width-8, 8, self.view.window.frame.size.width-16, 450);
    cell.tableCellBG.layer.borderColor = [[UIColor colorWithRed:0.847 green:0.847 blue:0.847 alpha:1.0] CGColor];
    cell.tableCellBG.layer.borderWidth = 1.0;
    cell.tableCellBG.layer.cornerRadius = 5.0;
    cell.tableCellBG.clipsToBounds = YES;
    

    UIBezierPath *maskPath;
    maskPath = [UIBezierPath bezierPathWithRoundedRect:cell.topContentBackground.bounds
                                     byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight)
                                           cornerRadii:CGSizeMake(5.0, 5.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = cell.topContentBackground.bounds;
    maskLayer.path = maskPath.CGPath;
    cell.topContentBackground.layer.mask = maskLayer;
    
    cell.productProfileImage.layer.cornerRadius = 20;
    cell.productProfileImage.clipsToBounds = YES;
    
    cell.productProfileImage.image = [UIImage imageNamed:[self.productProfilePic objectAtIndex:indexPath.row]];
    cell.productName.text = [self.productName objectAtIndex:indexPath.row];
    cell.productLikeCount.text = [self.productLikeCount objectAtIndex:indexPath.row];
    cell.productMessage.text = [self.productMessage objectAtIndex:indexPath.row];
    cell.productSchool.text = [self.productSchool objectAtIndex:indexPath.row];

    NSLog(@"%@,indexPath.row:::%ld", [self.productImage objectAtIndex:indexPath.row],indexPath.row);
    UIImage *image = [UIImage imageNamed:[self.productImage objectAtIndex:indexPath.row]];
    UIImage *squareImage = squareCropImageToSideLength(image, self.view.frame.size.width-31);
    
    UIImageView *productImage =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, squareImage.size.width, squareImage.size.height)];
    productImage.image=squareImage;
    [cell.productImageView addSubview:productImage];
            
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImage:)];
    tap.numberOfTapsRequired = 1;
    [cell.productImageView addGestureRecognizer:tap];


//    if ([[self.productImage2 objectAtIndex:indexPath.row] isEqualToString:@""]) {
//        UIImage *image1 = [UIImage imageNamed:[self.productImage objectAtIndex:indexPath.row]];
//        UIImageView *productImage1 =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-24, cell.productImageView.frame.size.height)];
//        productImage1.image=image1;
//        [cell.productImageView addSubview:productImage1];
//    }
//    else{
//        UIImage *image1 = [UIImage imageNamed:[self.productImage objectAtIndex:indexPath.row]];
//        UIImageView *productImage1 =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, (self.view.frame.size.width-24)/2, cell.productImageView.frame.size.height)];
//        productImage1.clipsToBounds = YES;
//        productImage1.image=image1;
//        [cell.productImageView addSubview:productImage1];
//        
//        UIImage *image2 = [UIImage imageNamed:[self.productImage2 objectAtIndex:indexPath.row]];
//        UIImageView *productImage2 =[[UIImageView alloc]initWithFrame:CGRectMake((self.view.frame.size.width-24)/2+12, 0, (self.view.frame.size.width-24)/2, cell.productImageView.frame.size.height)];
//        productImage2.clipsToBounds = YES;
//        productImage2.image=image2;
//        [cell.productImageView addSubview:productImage2];
//    }
    
    UIButton *editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    editBtn.frame = CGRectMake(self.view.window.frame.size.width-40, 15, 25, 25);
    UIImage *btnImage = [UIImage imageNamed:@"edit-icon.png"];
    [editBtn setImage:btnImage forState:UIControlStateNormal];
    [editBtn addTarget:self action:@selector(editBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:editBtn];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"didSelectRowAtIndexPath,indexPath.row:::%ld",indexPath.row);
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:250.f/255.f alpha:1.0f];
   }

-(void)tapImage :(id) sender{
    UITapGestureRecognizer *gesture = (UITapGestureRecognizer *) sender;
    NSLog(@"Tag::%ld", gesture.view.tag);
    NSMutableArray *photos = [NSMutableArray new];
    
    for (int i=0; i<self.imageArray.count; i++) {
        IDMPhoto *photo = [IDMPhoto photoWithImage:[self.imageArray objectAtIndex:i]];
        [photos addObject:photo];
    }
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photos];
    [self presentViewController:browser animated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void) editBtnAction:(UIButton*)sender{
    NSLog(@"editBtnAction");
    [self performSegueWithIdentifier:@"goEdit" sender:self];
}
#pragma mark - Square-Cropping Images
UIImage *squareCropImageToSideLength(UIImage *sourceImage,CGFloat sideLength){
    
    CGSize inputSize = sourceImage.size;
    sideLength = ceilf(sideLength);
    CGSize outputSize = CGSizeMake(sideLength, sideLength);
    CGFloat scale = MAX(sideLength / inputSize.width,
                        sideLength / inputSize.height);
    CGSize scaledInputSize = CGSizeMake(inputSize.width * scale,
                                        inputSize.height * scale);
    CGPoint center = CGPointMake(outputSize.width/2.0,
                                 outputSize.height/2.0);
    CGRect outputRect = CGRectMake(center.x - scaledInputSize.width/2.0,
                                   center.y - scaledInputSize.height/2.0,
                                   scaledInputSize.width,
                                   scaledInputSize.height);
    UIGraphicsBeginImageContextWithOptions(outputSize, YES, 0);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetInterpolationQuality(ctx, kCGInterpolationHigh);
    [sourceImage drawInRect:outputRect];
    UIImage *outImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return outImage;
}
@end
