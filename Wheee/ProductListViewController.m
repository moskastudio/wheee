//
//  ProductListViewController.m
//  Wheee
//
//  Created by Hungchi on 1/29/16.
//  Copyright © 2016 Hungchi. All rights reserved.
//

#import "ProductListViewController.h"
#import "ProductsViewController.h"
#import "ProductTableViewCell.h"

@interface ProductListViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property NSMutableArray *dataArray;
@property NSMutableArray *productName;
@property NSMutableArray *productSchool;
@property NSMutableArray *likeCount;
@property NSMutableArray *recordCount;
@property NSMutableArray *promoteOrNot;

@end

@implementation ProductListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ([self.titleText isEqualToString:@"goldDots"]) {
        self.dataArray = [[NSMutableArray alloc] initWithObjects:@"sample",@"sample2",@"sample4", nil];
        self.productName = [[NSMutableArray alloc] initWithObjects:@"小明與小美",@"爆炸頭阿蟹",@"123", nil];
        self.productSchool = [[NSMutableArray alloc] initWithObjects:@"台灣科技大學",@"雲科大",@"123", nil];
        self.likeCount = [[NSMutableArray alloc] initWithObjects:@"1000",@"500",@"33", nil];
        self.recordCount = [[NSMutableArray alloc] initWithObjects:@"555",@"99",@"1000", nil];
        self.promoteOrNot = [[NSMutableArray alloc] initWithObjects:@"1",@"1",@"0", nil];
    }
    else if ([self.titleText isEqualToString:@"archieve"]) {
        self.dataArray = [[NSMutableArray alloc] initWithObjects:@"sample3",@"sample4", nil];
        self.productName = [[NSMutableArray alloc] initWithObjects:@"Monster Channel",@"Between Journey", nil];
        self.productSchool = [[NSMutableArray alloc] initWithObjects:@"台科大",@"台灣科技大學", nil];
        self.likeCount = [[NSMutableArray alloc] initWithObjects:@"100",@"500", nil];
        self.recordCount = [[NSMutableArray alloc] initWithObjects:@"1000",@"333", nil];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onCategoryChangedSuccessNotification:) name:CategoryChangedSuccessNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)onCategoryChangedSuccessNotification:(NSNotification*) notify {
    self.dataArray = notify.object;
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 85;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *productTableIdentifier = @"productCell";
    ProductTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:productTableIdentifier];
    if (cell == nil) {
        cell = [[ProductTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:productTableIdentifier];
    }
    cell.productImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [self.dataArray objectAtIndex:indexPath.row]]];
    cell.productName.text = [self.productName objectAtIndex:indexPath.row];
    cell.productSchool.text = [self.productSchool objectAtIndex:indexPath.row];
    cell.likeCount.text = [self.likeCount objectAtIndex:indexPath.row];
    cell.recordCount.text = [self.recordCount objectAtIndex:indexPath.row];
    if ([[self.promoteOrNot objectAtIndex:indexPath.row] isEqualToString:@"1"]) {
        cell.promoteImage.hidden = NO;
    }
    else{
        cell.promoteImage.hidden = YES;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"showDetail" sender:self];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
