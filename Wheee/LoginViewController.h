//
//  LoginViewController.h
//  Wheee
//
//  Created by Hungchi on 1/29/16.
//  Copyright © 2016 Hungchi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface LoginViewController : UIViewController<FBSDKLoginButtonDelegate,FBSDKLoginTooltipViewDelegate>

@property (strong, nonatomic) IBOutlet UIButton *skipBtn;
@property (strong, nonatomic) IBOutlet FBSDKProfilePictureView *ProfilePictureView;
@property (weak, nonatomic) IBOutlet FBSDKLoginButton *loginButton;
@property (readonly, copy, nonatomic) NSString *userID;
@end
