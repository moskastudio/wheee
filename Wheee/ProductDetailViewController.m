//
//  ProductDetailViewController.m
//  Wheee
//
//  Created by Hungchi on 2/20/16.
//  Copyright © 2016 Hungchi. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "AppDelegate.h"
#import "CheckinViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface ProductDetailViewController ()

@property NSInteger captionLabelHeight;
@property NSInteger touchCaption;
@property NSInteger commentDetailsHeight;
@property NSInteger commentTag;
@property NSInteger commentFlag;
@property NSInteger collectionBtnFlag;
@property NSInteger recordBtnFlag;
@property NSInteger moreBtnFlag;
@property UILabel *captionLabel;

@property UIButton *moreBtn;
@property UIButton *collectionBtn;
@property UIButton *recordBtn;
@property UIButton *FBPage;
@property UIButton *FBShare;

@property UIView *tagView;
@property UIView *FBView;
@property UIView *lastCellView;
@property UIView *noticeCoverView;

@property NSMutableArray *selectedCommentArray;
@property NSString *dataString;
@property NSInteger test;
@property CGFloat *viewWidth;

- (IBAction)backAction:(id)sender;

@end

@implementation ProductDetailViewController{
    NSArray *profilePic;
    NSArray *name;
    NSArray *message;
}

- (void)viewDidLoad {
    self.moreBtnFlag = 0;
    self.collectionBtnFlag = 0;
    self.recordBtnFlag = 0;
    self.touchCaption = 0;
    [super viewDidLoad];
    
    NSLog(@"%@,%ld",_data,self.recordBtnFlag);
    if ([_data isEqualToString:@"save"]) {
        self.recordBtnFlag = 2;
        [self.productDetailTableView reloadData];
    }
    else if([_data isEqualToString:@"cancel"]) {
        self.recordBtnFlag = 1;
        [self.productDetailTableView reloadData];
    }
    else{
        [self.recordBtn setBackgroundImage:[UIImage imageNamed:@"btn-record-n@2x.png"]
                                  forState:UIControlStateNormal];
    }
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.FB_UserID = [FBSDKProfile currentProfile].userID;
    appDelegate.FB_UesrName = [FBSDKProfile currentProfile].name;
    NSLog(@"留言:::%@,%@",appDelegate.FB_UserID,appDelegate.FB_UesrName);
    
    self.selectedCommentArray = [[NSMutableArray alloc] init];
    self.commentField.delegate = self;
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:gestureRecognizer];
    gestureRecognizer.cancelsTouchesInView = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    profilePic = [NSArray arrayWithObjects:@"pic-蕭@2x.png", @"pic-羅@2x.png", @"pic-katherine@2x.png",@"pic-格子襯衫@2x.png",@"pic-盧毛毛@2x.png",nil];
    name = [NSArray arrayWithObjects:@"蕭景蒙",@"Katherine Cruz",@"羅希文",@"格子襯衫",@"盧毛毛",nil];
    message =[NSArray arrayWithObjects:@"這個題目選得很有意義意義意義意義。",@"AaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbCCCCCCC",@"視障者app能做成這樣很不簡單呢！",@"Between Journey 結合了語音導覽，地點報位的技術，將平時搭乘的交通工具，以最適合的方式串聯在視障者的使用中",@"在熙攘的生活中，我們感受到大眾交通帶來的便利，享受著再熟悉不過的舒適體驗以及交通串聯。而同樣生活在這個城市的另一群人，他們缺乏視覺的輔助，該如何在城市中漫遊移動？Between Journey 是一款協助視障者交通轉乘以及提供交通資訊的APP",nil];
}

- (void)viewDidAppear:(BOOL)animated{
    NSLog(@"%@,%ld",_data,self.recordBtnFlag);
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    if ([appDelegate.RecordDiscoverBtn isEqualToString:@"recordBtn"]) {
        self.noticeCoverView = [[UIView alloc] initWithFrame:CGRectMake(0,65, self.view.window.frame.size.width,self.view.window.frame.size.height-65)];
        self.noticeCoverView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
        
        UIButton *noticeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        noticeBtn.frame = CGRectMake((self.view.window.frame.size.width/4)*3-32, self.view.window.frame.size.height-166, 64, 52);
        [noticeBtn setBackgroundImage:[UIImage imageNamed:@"bg-notice@2x.png"]
                             forState:UIControlStateNormal];
        [noticeBtn addTarget:self action:@selector(noticeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.noticeCoverView addSubview:noticeBtn];
        [self.view addSubview:self.noticeCoverView];
    }
}
//-(void)viewDidLayoutSubviews {
//    [super viewDidLayoutSubviews];
//    // Scroll to the bottom
//    NSLog(@"viewDidLayoutSubviews");
//    [self.scrollView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
//}

// Called when the UIKeyboardDidShowNotification is sent.

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return profilePic.count+4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell = nil;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.clipsToBounds = YES;
    
    if (indexPath.row == 0) {
        NSString *website = @"https://youtu.be/sYqqn1jKSpA";
        NSURL *url = [NSURL URLWithString:website];
        NSURLRequest *requestURL =[NSURLRequest requestWithURL:url];
        
        CGRect webFrame = CGRectMake(8.0, 0.0, self.view.window.frame.size.width-16, self.view.window.frame.size.width/1.5);
        UIWebView *webView = [[UIWebView alloc] initWithFrame:webFrame];
        webView.backgroundColor = [UIColor colorWithRed:1 green:1 blue:0.98 alpha:1.0f];
        webView.scalesPageToFit = YES;
        webView.autoresizesSubviews = YES;
        [webView loadRequest:requestURL];
        [cell addSubview:webView];
        
        self.tagView = [[UIView alloc] initWithFrame:CGRectMake(self.view.window.frame.size.width-54, 0, 54, (self.view.window.frame.size.width-16)/1.3)];
        self.tagView.backgroundColor = [UIColor clearColor];
        for (int i=0; i<3; i++) {
            if (i==0) {
                UIImage *tagImage=[UIImage imageNamed:@"btn-highlight-tag@2x.png"];
                UIImageView *tagImageView =[[UIImageView alloc]initWithFrame:CGRectMake(0, 10+38*i, 54, 28)];
                tagImageView.image=tagImage;
                UILabel *tagLabel=[[UILabel alloc] initWithFrame:CGRectMake(15, 8, 24, 12)];
                [tagLabel setFont:[UIFont fontWithName:@"PingFang TC" size:12]];
                tagLabel.textColor = [UIColor colorWithRed:(74/255.f) green:(74/255.f) blue:(74/255.f) alpha:1];
                tagLabel.text = @"入圍";
                [tagImageView addSubview:tagLabel];
                [self.tagView addSubview:tagImageView];
            }
            else{
                UIImage *tagImage=[UIImage imageNamed:@"btn-normal-tag@2x.png"];
                UIImageView *tagImageView =[[UIImageView alloc]initWithFrame:CGRectMake(0, 10+38*i, 54, 28)];
                tagImageView.image=tagImage;
                UILabel *tagLabel=[[UILabel alloc] initWithFrame:CGRectMake(15, 8, 24, 12)];
                [tagLabel setFont:[UIFont fontWithName:@"PingFang TC" size:12]];
                tagLabel.textColor = [UIColor colorWithRed:(74/255.f) green:(74/255.f) blue:(74/255.f) alpha:1];
                tagLabel.text = @"APP";
                [tagImageView addSubview:tagLabel];
                [self.tagView addSubview:tagImageView];
            }
        }
        cell.textLabel.text = @"";
        cell.detailTextLabel.text=@"";
        [cell addSubview:self.tagView];
        cell.contentView.backgroundColor = [UIColor colorWithRed:1 green:1 blue:0.98 alpha:1.0f];
        self.productDetailTableView.alwaysBounceVertical= NO;
    }
    else if (indexPath.row == 1) {
        self.captionLabel = [[UILabel alloc]initWithFrame:CGRectMake(8, 0, self.view.window.frame.size.width-16, 65)];
        self.captionLabel.numberOfLines = 0;
        self.captionLabel.font = [UIFont fontWithName:@"PingFang TC" size:14.0f];
        self.captionLabel.textColor = [UIColor colorWithRed:0.29 green:0.29 blue:0.29 alpha:1.0];
        self.captionLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.captionLabel.text = @"在熙攘的生活中，我們感受到大眾交通帶來的便利，享受著再熟悉不過的舒適體驗以及交通串聯。而同樣生活在這個城市的另一群人，他們缺乏視覺的輔助，該如何在城市中漫遊移動？Between Journey 是一款協助視障者交通轉乘以及提供交通資訊的APP，利用 Voiceover 及 iBeacon 這兩項技術輔助視障者自由行走。當視障者持有手機接近Beacon時，手機將能判斷視障者當下的位置，並給予精確的環境訊息。Between Journey 結合了語音導覽，地點報位的技術，將平時搭乘的交通工具，以最適合的方式串聯在視障者的使用中。";
        CGSize size = [self.captionLabel sizeThatFits:CGSizeMake(self.captionLabel.frame.size.width, MAXFLOAT)];
        self.captionLabelHeight = size.height+8;
        self.captionLabel.frame =CGRectMake(8, 0, self.view.window.frame.size.width-16, self.captionLabelHeight);
        cell.textLabel.text = @"";
        cell.detailTextLabel.text=@"";
        [cell addSubview:self.captionLabel];
        cell.contentView.backgroundColor = [UIColor colorWithRed:1 green:1 blue:0.98 alpha:1.0f];
    }
    else if (indexPath.row == 2){
        UIImage *line = [UIImage imageNamed:@"line@2x.png"];
        UIImageView *lineImage =[[UIImageView alloc]initWithFrame:CGRectMake(8, 34, self.view.window.frame.size.width-16, 2)];
        lineImage.image=line;
        [cell addSubview:lineImage];
        
        self.collectionBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        self.collectionBtn.frame = CGRectMake(12, 2, 28, 26);
        if (self.collectionBtnFlag == 0) {
            [self.collectionBtn setBackgroundImage:[UIImage imageNamed:@"btn-like-n.png"]
                                          forState:UIControlStateNormal];
        }
        else{
            [self.collectionBtn setBackgroundImage:[UIImage imageNamed:@"btn-like-p.png"]
                                          forState:UIControlStateNormal];
        }
        [self.collectionBtn addTarget:self action:@selector(collectionBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:self.collectionBtn];
        
        self.recordBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        self.recordBtn.frame = CGRectMake(88, 2, 26, 26);
        if (self.recordBtnFlag == 0) {
            [self.recordBtn setBackgroundImage:[UIImage imageNamed:@"btn-note-n.png"]
                                      forState:UIControlStateNormal];
        }
        else if (self.recordBtnFlag == 1){
            [self.recordBtn setBackgroundImage:[UIImage imageNamed:@"btn-note-n.png"]
                                      forState:UIControlStateNormal];
        }
        else if (self.recordBtnFlag == 2){
            [self.recordBtn setBackgroundImage:[UIImage imageNamed:@"btn-note-p.png"]
                                      forState:UIControlStateNormal];
        }
        
        [self.recordBtn addTarget:self action:@selector(recordBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:self.recordBtn];
        
        self.moreBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        self.moreBtn.frame = CGRectMake(self.view.window.frame.size.width-34, 2, 26, 26);
        if (self.moreBtnFlag == 0) {
            [self.moreBtn setImage:[UIImage imageNamed:@"btn-more-n.png"] forState:UIControlStateNormal];
        }
        else{
            [self.moreBtn setImage:[UIImage imageNamed:@"btn-more-p.png"] forState:UIControlStateNormal];
        }
        [self.moreBtn addTarget:self action:@selector(moreBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:self.moreBtn];
        
        self.FBView = [[UIView alloc] initWithFrame:CGRectMake(0, 36, self.view.window.frame.size.width, 35)];
        
        self.FBPage = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        self.FBPage.frame = CGRectMake(self.view.window.frame.size.width*0.25-65, 0, 130, 35);
        [self.FBPage setBackgroundImage:[UIImage imageNamed:@"btn-facebook-fanpage.png"]
                               forState:UIControlStateNormal];
        [self.FBPage addTarget:self action:@selector(FBPageAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.FBView addSubview:self.FBPage];
        
        self.FBShare = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        self.FBShare.frame = CGRectMake(self.view.window.frame.size.width*0.75-59, 0, 119, 35);
        [self.FBShare setBackgroundImage:[UIImage imageNamed:@"btn-facebook-share.png"]
                                forState:UIControlStateNormal];
        [self.FBShare addTarget:self action:@selector(FBShareAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.FBView addSubview:self.FBShare];
        
        UIImage *line2 = [UIImage imageNamed:@"line@2x.png"];
        UIImageView *lineImage2 =[[UIImageView alloc]initWithFrame:CGRectMake(8, 31, self.view.window.frame.size.width-16, 2)];
        lineImage2.image=line2;
        [self.FBView addSubview:lineImage2];
        [cell addSubview:self.FBView];
        
        cell.textLabel.text = @"";
        cell.detailTextLabel.text=@"";
        cell.contentView.backgroundColor = [UIColor colorWithRed:1 green:1 blue:0.98 alpha:1.0f];
    }
    else if(indexPath.row == profilePic.count+3){
        self.lastCellView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.window.frame.size.width, 20)];
        
        UIButton *moreCommentBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        moreCommentBtn.frame = CGRectMake(self.view.window.frame.size.width/2-13, 4, 26, 13);
        [moreCommentBtn setBackgroundImage:[UIImage imageNamed:@"btn-more-comment@2x.png"]
                                  forState:UIControlStateNormal];
        [moreCommentBtn addTarget:self action:@selector(FBPageAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.lastCellView addSubview:moreCommentBtn];
        
        UIButton *goTopBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        goTopBtn.frame = CGRectMake(self.view.window.frame.size.width-23, 1, 15, 19);
        [goTopBtn setBackgroundImage:[UIImage imageNamed:@"btn-go-top@2x.png"]
                            forState:UIControlStateNormal];
        [goTopBtn addTarget:self action:@selector(goTopAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.lastCellView addSubview:goTopBtn];
        [cell addSubview:self.lastCellView];
        cell.textLabel.text = @"";
        cell.detailTextLabel.text=@"";
        cell.contentView.backgroundColor = [UIColor colorWithRed:1 green:1 blue:0.98 alpha:1.0f];
    }
    else if (indexPath.row >= 3) {
        cell.imageView.layer.cornerRadius = 15;
        cell.imageView.layer.borderWidth = 1.0f;
        cell.imageView.layer.borderColor = [UIColor blackColor].CGColor;
        cell.imageView.clipsToBounds = YES;
        [cell.imageView setFrame:CGRectMake(cell.imageView.frame.origin.x, cell.imageView.frame.origin.y, 30, 30)];
        cell.imageView.image = [UIImage imageNamed:[profilePic objectAtIndex:indexPath.row-3]];
        cell.textLabel.text = [name objectAtIndex:indexPath.row-3];
        cell.textLabel.textColor = [UIColor colorWithRed:0.13 green:0.24 blue:0.4 alpha:1.0];
        cell.textLabel.font = [UIFont fontWithName:@"PingFang TC Bold" size:14.0f];
        cell.contentView.backgroundColor = [UIColor colorWithRed:1 green:1 blue:0.98 alpha:1.0f];
        //if (indexPath.row == self.commentTag) {
        cell.detailTextLabel.alpha = 1;
        cell.imageView.alpha = 1;
        if ([self.selectedCommentArray containsObject:[NSNumber numberWithLong:indexPath.row]]) {
            cell.detailTextLabel.numberOfLines = 0;
            cell.detailTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
            cell.detailTextLabel.text = [message objectAtIndex:indexPath.row-3];
            cell.detailTextLabel.textColor = [UIColor colorWithRed:0.29 green:0.29 blue:0.29 alpha:1.0];
            cell.detailTextLabel.font = [UIFont fontWithName:@"PingFang TC" size:14.0f];
            cell.detailTextLabel.clipsToBounds = YES;
        }
        else{
            cell.detailTextLabel.text = [message objectAtIndex:indexPath.row-3];
            cell.detailTextLabel.textColor = [UIColor colorWithRed:0.29 green:0.29 blue:0.29 alpha:1.0];
            cell.detailTextLabel.font = [UIFont fontWithName:@"PingFang TC" size:14.0f];
            cell.detailTextLabel.numberOfLines = 1;
            cell.detailTextLabel.clipsToBounds = YES;
        }
    }
    
    [self.productDetailTableView setSeparatorColor:[UIColor colorWithRed:255 green:255 blue:250 alpha:1]];
    //    cell.tag = indexPath.row;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:250.f/255.f alpha:1.0f];
    
    if (indexPath.row == 1) {
        if (self.touchCaption == 0) {
            self.touchCaption = 1;
        }
        else{
            self.touchCaption = 0;
        }
        [self.captionLabel removeFromSuperview];
        
        NSIndexPath *reloadIndexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        [self.productDetailTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:reloadIndexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
    else{
        self.commentTag = indexPath.row;
        if ([self.selectedCommentArray containsObject:[NSNumber numberWithLong:indexPath.row]]) {
            [self.selectedCommentArray removeObject:[NSNumber numberWithLong:indexPath.row]];
        }
        else{
            [self.selectedCommentArray addObject:[NSNumber numberWithLong:indexPath.row]];
        }
        
        cell.detailTextLabel.alpha = 0;
        cell.imageView.alpha = 0;
        NSLog(@"self.commentTag::%ld",self.commentTag);
        NSIndexPath *reloadIndexPath = [NSIndexPath indexPathForRow:self.commentTag inSection:0];
        [self.productDetailTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:reloadIndexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == 0){
        return self.view.window.frame.size.width/1.5;
    }
    else if(indexPath.row == 1){
        if (self.touchCaption == 0) {
            return 65;
        }
        else
            return self.captionLabelHeight;
    }
    else if (indexPath.row == 2){
        if (self.moreBtnFlag == 0) {
            return 36;
        }
        else
            return 70;
    }
    else if (indexPath.row == profilePic.count+3){
        return 20;
    }
    else if ([self.selectedCommentArray containsObject:[NSNumber numberWithLong:indexPath.row]]) {
        NSString *text = [message objectAtIndex:indexPath.row-3];
        CGSize constraintSize = CGSizeMake(330.0f, MAXFLOAT);
        UIFont *cellFont = [UIFont fontWithName:@"PingFang TC" size:14.0];
        
        CGRect textRect = [text boundingRectWithSize:constraintSize
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:@{NSFontAttributeName:cellFont}
                                             context:nil];
        CGSize size = textRect.size;
        return size.height+35;
    }
    else
        return 54.6;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


 #pragma mark - Navigation
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     if ([[segue identifier] isEqualToString:@"goRecoverView"]) {
         CheckinViewController *checkinViewController = [segue destinationViewController];
         checkinViewController.theSchool = @"台科大"; // Set the exposed property
         checkinViewController.theProduct = @"爆炸頭阿蟹";
     }
     
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }


- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) FBPageAction:(UIButton*)sender{
    if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/depressytrouble"]]) {
        // opening the app didn't work - let's open Safari
        if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/wheeedesign"]]) {
            // nothing works - perhaps we're not online
            NSLog(@"Note to self: find a different hobby");
        }
    }
}
-(void) FBShareAction:(UIButton*)sender{
    
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:@"https://www.facebook.com/wheeedesign/?fref=ts"];
    [FBSDKShareDialog showFromViewController:self
                                 withContent:content
                                    delegate:nil];
    /*
    // 判斷社群網站的服務可用
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *mySocialComposeView = [[SLComposeViewController alloc] init];
        mySocialComposeView=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        // 插入文字
        [mySocialComposeView setInitialText:@"測試"];
        // 插入網址
        NSURL *myURL = [[NSURL alloc] initWithString:@"https://www.facebook.com/wheeedesign/?fref=ts"];
        [mySocialComposeView addURL: myURL];
        // 插入圖片
        [mySocialComposeView addImage:[UIImage imageNamed:@"loginPage.png"]];
        // 呼叫建立的SocialComposeView
        [self presentViewController:mySocialComposeView animated:YES completion:^{
            NSLog(@"Success SocialComposeView");
        }];
        // 訊息成功送出與否的之後處理
        [mySocialComposeView setCompletionHandler:^(SLComposeViewControllerResult result){
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Cancel");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Success");
                    break;
                default:
                    NSLog(@"Error");
                    break;
            }
        }];
    }
    else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"請先在系統設定中登入Facebook" delegate:nil cancelButtonTitle:@"好" otherButtonTitles:nil, nil];
        [alertView show];
    }
     */
}
-(void) collectionBtnAction:(UIButton*)sender{
    if (self.collectionBtnFlag == 0) {
        [self.collectionBtn setBackgroundImage:[UIImage imageNamed:@"btn-like-p.png"]
                                      forState:UIControlStateNormal];
        self.collectionBtnFlag = 1;
        self.noticeCoverView = [[UIView alloc] initWithFrame:CGRectMake(0,65, self.view.window.frame.size.width,self.view.window.frame.size.height-65)];
        self.noticeCoverView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
        
        UIButton *noticeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        noticeBtn.frame = CGRectMake((self.view.window.frame.size.width/4)*3-32, self.view.window.frame.size.height-166, 64, 52);
        [noticeBtn setBackgroundImage:[UIImage imageNamed:@"bg-notice@2x.png"]
                             forState:UIControlStateNormal];
        [noticeBtn addTarget:self action:@selector(noticeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.noticeCoverView addSubview:noticeBtn];
        [self.view addSubview:self.noticeCoverView];
    }
    else{
        [self.collectionBtn setBackgroundImage:[UIImage imageNamed:@"btn-like-n.png"]
                                      forState:UIControlStateNormal];
        self.collectionBtnFlag = 0;
    }
}
-(void) recordBtnAction:(UIButton*)sender{
//    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    [self performSegueWithIdentifier:@"goRecoverView" sender:self];
}
-(void) moreBtnAction:(UIButton*)sender{
    NSLog(@"moreBtnAction");
    if (self.moreBtnFlag == 0) {
        UIImage *btnImage = [UIImage imageNamed:@"btn-more-p.png"];
        [self.moreBtn setImage:btnImage forState:UIControlStateNormal];
        self.moreBtnFlag = 1;
    }
    else{
        self.moreBtnFlag = 0;
        [self.moreBtn setImage:[UIImage imageNamed:@"btn-more-n.png"] forState:UIControlStateHighlighted];
        [self.FBView removeFromSuperview];
    }
    
    NSIndexPath *reloadIndexPath = [NSIndexPath indexPathForRow:2 inSection:0];
    [self.productDetailTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:reloadIndexPath] withRowAnimation:UITableViewRowAnimationNone];
}
-(void) goTopAction:(UIButton*)sender{
    NSLog(@"goTopAction");
    [self.productDetailTableView setContentOffset:CGPointMake(0,0) animated:YES];
}
-(void) noticeBtnAction:(UIButton*) sender{
    [self.noticeCoverView removeFromSuperview];
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.RecordDiscoverBtn = @"";
}
- (void)keyboardWasShown:(NSNotification*)aNotification{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height-45, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.commentField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, self.commentField.frame.origin.y-kbSize.height);
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}
- (BOOL)textFieldShouldReturn:(UITextField *)commentField {
    [commentField resignFirstResponder];
    [self.view endEditing:YES];
    return YES;
}
- (void) hideKeyboard {
    [self.commentField resignFirstResponder];
}

@end
