//
//  ProductRecordViewController.h
//  Wheee
//
//  Created by Hungchi on 3/14/16.
//  Copyright © 2016 Hungchi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IDMPhotoBrowser.h"

@interface ProductRecordViewController : UIViewController
<UITableViewDataSource, UITableViewDelegate,IDMPhotoBrowserDelegate>

@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSString *imageFile;
@property (strong, nonatomic) IBOutlet UITableView *productRecordTableView;
@property (strong, nonatomic) IBOutlet UIButton *goEditButton;
@end
