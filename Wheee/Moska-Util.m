//
//  Moska-Util.m
//  CardofPNN
//
//  Created by Osiris on 11/22/14.
//  Copyright (c) 2014 moskastudio. All rights reserved.
//

#import "Moska-Util.h"

@implementation Moska_Util

+ (CGFloat) getScreenWidth
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    return screenWidth;
}

+ (CGFloat) getScreenHeight
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    
    return screenHeight;
}

+ (NSString*) getUUID{
    UIDevice *device = [UIDevice currentDevice];
    NSString  *currentDeviceId = [[device identifierForVendor] UUIDString];
    NSLog(@"currentDeviceId:%@", currentDeviceId);
    return currentDeviceId;
}

+ (NSString*) sha1:(NSString*) input
{
    
    const char* cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData* data = [NSData dataWithBytes:cstr length:input.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, (CC_LONG)data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
}

+ (void)saveImage:(UIImage*)image andFilename:(NSString*) filename
{
    if (image != nil)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:filename];
        NSData* data = UIImagePNGRepresentation(image);
        [data writeToFile:path atomically:YES];
    }
}

+ (UIImage*)loadImage:(NSString*) filename
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:filename];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}


+(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (CGSize)newSizeOfImage: (UIImage*) sourceImage andWidth: (float) i_width
{
    NSLog(@"newSizeOfImage - original:%@", NSStringFromCGSize(sourceImage.size));
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    NSLog(@"newSizeOfImage - new:%@", NSStringFromCGSize(CGSizeMake(newWidth, newHeight)));

    return CGSizeMake(newWidth, newHeight);
}

+ (UIImageView*)getLoadingView {
    NSLog(@"getLoadingView");
    UIImageView* animatedImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 67)];
    animatedImageView.animationImages = [NSArray arrayWithObjects:
                                         [UIImage imageNamed:@"loading01.png"],
                                         [UIImage imageNamed:@"loading02.png"],
                                         [UIImage imageNamed:@"loading03.png"],
                                         [UIImage imageNamed:@"loading04.png"],
                                         [UIImage imageNamed:@"loading05.png"],
                                         [UIImage imageNamed:@"loading06.png"],nil];
    animatedImageView.animationDuration = 1.0f;
    animatedImageView.animationRepeatCount = 0;
    [animatedImageView startAnimating];
    
    return animatedImageView;
}

+ (NSMutableArray*) checkCategoryOrder:(NSMutableArray*) newList
{
    NSMutableArray* orderedList = [Moska_Util fixMiniArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"MyCategoryList"]];
    @try
    {
        NSMutableArray* orderedCategories = [[NSMutableArray alloc] init];
        for (int i = 0; i < [[[orderedList objectAtIndex:0] objectForKey:@"SubCategory"] count]; i++)
        {
            [orderedCategories addObject:[[[[orderedList objectAtIndex:0] objectForKey:@"SubCategory"] objectAtIndex:i] objectForKey:@"categoryName"]];
        }
        
        NSMutableArray* newCategories = [[NSMutableArray alloc] init];
        for (int i = 0; i < [[[newList objectAtIndex:0] objectForKey:@"SubCategory"] count]; i++)
        {
            [newCategories addObject:[[[[newList objectAtIndex:0] objectForKey:@"SubCategory"] objectAtIndex:i] objectForKey:@"categoryName"]];
        }
        
        // remove non-exist object
        for (int j = 0; j < [orderedCategories count]; j++)
        {
            if (![newCategories containsObject:[orderedCategories objectAtIndex:j]])
            {
                NSLog(@"remove:%@", [orderedCategories objectAtIndex:j]);
                [orderedCategories removeObject:[orderedCategories objectAtIndex:j]];
            }
        }
        
        // append new object
        for (int j = 0; j < [newCategories count]; j++)
        {
            if (![orderedCategories containsObject:[newCategories objectAtIndex:j]])
            {
                NSLog(@"add:%@", [newCategories objectAtIndex:j]);
                [orderedCategories addObject:[newCategories objectAtIndex:j]];
            }
        }
        
        // remove old orderedList
        [[[orderedList objectAtIndex:0] objectForKey:@"SubCategory"] removeAllObjects];
        
        // append new orderedList
        for (int k = 0; k < [orderedCategories count]; k++)
        {
            for (NSMutableDictionary* category in [[newList objectAtIndex:0] objectForKey:@"SubCategory"])
            {
                if ([[orderedCategories objectAtIndex:k] isEqualToString:[category objectForKey:@"categoryName"]])
                {
                    [[[orderedList objectAtIndex:0] objectForKey:@"SubCategory"] addObject:category];
                }
            }
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:orderedList forKey:@"MyCategoryList"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    @catch (NSException* exception)
    {
        NSLog(@"MyException - checkCategoryOrder:%@", exception.description);
    }
    return orderedList;
}

+ (NSMutableArray*) fixMiniArray:(NSMutableArray*) inputArray
{
    NSMutableArray* outputArray = [[NSMutableArray alloc] init];
    for (NSMutableDictionary* dict1 in inputArray)
    {
        NSMutableDictionary* new_dict1 = [[NSMutableDictionary alloc] init];
        NSMutableArray* new_array2 = [[NSMutableArray alloc] init];
        for (NSMutableArray* array2 in [dict1 objectForKey:@"SubCategory"])
        {
            [new_array2 addObject:array2];
        }
        [new_dict1 setObject:new_array2 forKey:@"SubCategory"];
        [new_dict1 setObject:[dict1 objectForKey:@"categoryName"] forKey:@"categoryName"];
        [new_dict1 setObject:[dict1 objectForKey:@"zh"] forKey:@"zh"];
        [outputArray addObject:new_dict1];
    }
    return outputArray;
}


+ (NSString*) getObjectFromNSDefaultForKey:(NSString*) key{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:key] == nil) {
        return @"";
    } else {
        return [[NSUserDefaults standardUserDefaults] objectForKey:key];
    }
}

@end
