//
//  AppDelegate.h
//  Wheee
//
//  Created by Hungchi on 1/28/16.
//  Copyright © 2016 Hungchi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    NSString *RecordDiscoverBtn;
    NSString *FB_UserID;
    NSString *FB_UesrName;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, retain) NSString *RecordDiscoverBtn;
@property (strong ,retain) NSString *FB_UserID;
@property (strong ,retain) NSString *FB_UesrName;


@end

