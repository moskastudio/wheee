//
//  ProductsViewController.m
//  Wheee
//
//  Created by Hungchi on 1/29/16.
//  Copyright © 2016 Hungchi. All rights reserved.
//

#import "ProductsViewController.h"
#import "ProductListViewController.h"
#import "ProductRecordViewController.h"

NSString *CategoryChangedSuccessNotification = @"CategoryChangedSuccessNotification";

@interface ProductsViewController ()

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;
@property (strong, nonatomic) ProductListViewController *currentViewController;
@property UIGestureRecognizer *tapper;

@property NSInteger currentIndex;

@property (weak, nonatomic) IBOutlet UIButton *goldDotsButton;
@property (weak, nonatomic) IBOutlet UIButton *archieveButton;
@property (weak, nonatomic) IBOutlet UIButton *checkinButton;

- (IBAction)goldDotsAction:(id)sender;
- (IBAction)archieveAction:(id)sender;
- (IBAction)checkinAction:(id)sender;

@end

@implementation ProductsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.pageTitles = @[@"goldDots", @"archieve", @"checkin"];
    self.pageImages = @[@"btn-all-n@2x.png", @"btn-like-Text-n@2x.png", @"btn-records-n@2x.png"];
    
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
    
    ProductListViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 98, self.view.frame.size.width, self.view.frame.size.height - 30);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    UIImage *btnImage = [UIImage imageNamed:@"btn-all-p@2x.png"];
    [self.goldDotsButton setImage:btnImage forState:UIControlStateSelected];
    
    //dismiss Keyboard tap
    self.tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    self.tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:self.tapper];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender{
    [self.view endEditing:YES];
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((ProductListViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((ProductListViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.pageTitles count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count])) {
        return nil;
    }
    
    if (index == 2) {
        // Create a new view controller and pass suitable data.
        ProductRecordViewController *productRecordViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"productRecordViewController"];
        productRecordViewController.imageFile = self.pageImages[index];
        productRecordViewController.titleText = self.pageTitles[index];
        productRecordViewController.pageIndex = index;
        return productRecordViewController;
    }
    else {
        // Create a new view controller and pass suitable data.
        ProductListViewController *productListViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductListViewController"];
        productListViewController.imageFile = self.pageImages[index];
        productListViewController.titleText = self.pageTitles[index];
        productListViewController.pageIndex = index;
        return productListViewController;
    }
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed {
    
    if (!completed)
    {
        return;
    }
    
    if (finished) {
        self.currentIndex = ((ProductListViewController *)[self.pageViewController.viewControllers objectAtIndex:0]).pageIndex;
        
        [self unselectAllButtons];
        if (self.currentIndex == 0) {
            self.goldDotsButton.selected = YES;
        }
        else if (self.currentIndex == 1) {
            self.archieveButton.selected = YES;
        }
        else if (self.currentIndex == 2) {
            self.checkinButton.selected = YES;
        }
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)unselectAllButtons{
    self.goldDotsButton.selected = NO;
    self.archieveButton.selected = NO;
    self.checkinButton.selected = NO;
}

- (IBAction)goldDotsAction:(id)sender {
    [self unselectAllButtons];
    self.goldDotsButton.selected = YES;
    UIImage *btnImage = [UIImage imageNamed:@"btn-all-p@2x.png"];
    [self.goldDotsButton setImage:btnImage forState:UIControlStateSelected];
    
    ProductListViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    if (self.currentIndex != 0) {
        [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
    }
    self.currentIndex = 0;
}

- (IBAction)archieveAction:(id)sender {
    [self unselectAllButtons];
    self.archieveButton.selected = YES;    UIImage *btnImage = [UIImage imageNamed:@"btn-like-Text-p@2x.png"];
    [self.archieveButton setImage:btnImage forState:UIControlStateSelected];
    
    ProductListViewController *startingViewController = [self viewControllerAtIndex:1];
    NSArray *viewControllers = @[startingViewController];
    if (self.currentIndex > 1) {
        [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
    }
    else if (self.currentIndex < 1) {
        [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    }
    self.currentIndex = 1;
}

- (IBAction)checkinAction:(id)sender {
    [self unselectAllButtons];
    self.checkinButton.selected = YES;
    UIImage *btnImage = [UIImage imageNamed:@"btn-records-p@2x.png"];
    [self.checkinButton setImage:btnImage forState:UIControlStateSelected];
    
    ProductListViewController *startingViewController = [self viewControllerAtIndex:2];
    NSArray *viewControllers = @[startingViewController];
    if (self.currentIndex != 2) {
        [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    }
    self.currentIndex = 2;
}
@end
