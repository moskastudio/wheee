//
//  AccountViewController.h
//  Wheee
//
//  Created by aisoter on 2016/4/19.
//  Copyright © 2016年 Hungchi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "AppDelegate.h"

@interface AccountViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,FBSDKAppInviteDialogDelegate>{
    AppDelegate* appDelegate;
}

@property (strong, nonatomic) IBOutlet UIImageView *porfileImage;
@property (strong, nonatomic) IBOutlet UIView *inviteFriendBackground;
@property (strong, nonatomic) IBOutlet UILabel *friendCountLabel;
@property (strong, nonatomic) IBOutlet UIButton *inviteFriendBtn;
- (IBAction)inviteFriendBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *friendsListTable;
- (IBAction)goSetting:(id)sender;

@end
