//
//  SettingViewController.h
//  Wheee
//
//  Created by aisoter on 2016/4/20.
//  Copyright © 2016年 Hungchi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,UINavigationBarDelegate,UINavigationControllerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)backBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *customNavBar;
@end
