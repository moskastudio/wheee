//
//  SchoolProductListTableViewCell.h
//  Wheee
//
//  Created by aisoter on 2016/3/21.
//  Copyright © 2016年 Hungchi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SchoolProductListTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *schoolList;
@property (strong, nonatomic) IBOutlet UILabel *productList;
@property (strong, nonatomic) IBOutlet UILabel *productTag;

@end
