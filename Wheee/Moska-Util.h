//
//  Moska-Util.h
//  CardofPNN
//
//  Created by Osiris on 11/22/14.
//  Copyright (c) 2014 moskastudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>

@interface Moska_Util : NSObject

+ (CGFloat) getScreenWidth;
+ (CGFloat) getScreenHeight;
+ (NSString*) getUUID;

+ (NSString*) sha1:(NSString*) input;

+ (void)saveImage:(UIImage*)image andFilename:(NSString*) filename;
+ (UIImage*)loadImage:(NSString*) filename;
+ (UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width;
+ (CGSize)newSizeOfImage: (UIImage*) sourceImage andWidth: (float) i_width;
+ (UIImageView*)getLoadingView;
+ (NSMutableArray*) checkCategoryOrder:(NSMutableArray*) newList;
+ (NSMutableArray*) fixMiniArray:(NSMutableArray*) inputArray;
+ (NSString*) getObjectFromNSDefaultForKey:(NSString*) key;


@end
