//
//  ProductDetailViewController.h
//  Wheee
//
//  Created by Hungchi on 2/20/16.
//  Copyright © 2016 Hungchi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "AppDelegate.h"

@class AppDelegate;
@interface ProductDetailViewController : UIViewController<UIWebViewDelegate,UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate,FBSDKSharingContent>{
    AppDelegate* appDelegate;
}

@property (strong, nonatomic) IBOutlet UITextField *commentField;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UITableView *productDetailTableView;
//@property (nonatomic, assign) id currentResponder;
@property (nonatomic, retain) NSString *data;
@end
