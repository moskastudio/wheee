//
//  ProductsViewController.h
//  Wheee
//
//  Created by Hungchi on 1/29/16.
//  Copyright © 2016 Hungchi. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString* CategoryChangedSuccessNotification;

@interface ProductsViewController : UIViewController
<UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@end
