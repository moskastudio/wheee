//
//  productRecordTableViewCell.h
//  Wheee
//
//  Created by aisoter on 2016/3/15.
//  Copyright © 2016年 Hungchi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface productRecordTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *productProfileImage;
@property (strong, nonatomic) IBOutlet UILabel *productName;
@property (strong, nonatomic) IBOutlet UILabel *productSchool;
@property (strong, nonatomic) IBOutlet UILabel *productLikeCount;
@property (strong, nonatomic) IBOutlet UILabel *productMessage;
@property (strong, nonatomic) IBOutlet UIImageView *tableCellBG;
@property (strong, nonatomic) IBOutlet UIView *productImageView;
@property (strong, nonatomic) IBOutlet UIView *topContentBackground;

@end
