//
//  MapViewController.m
//  Wheee
//
//  Created by Hungchi on 1/29/16.
//  Copyright © 2016 Hungchi. All rights reserved.
//

#import "MapViewController.h"

@interface MapViewController ()

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSArray *tabbarIconArray = [[NSArray alloc] initWithObjects:@"icn_tab_home.png", @"icn_tab_note.png", @"icn_tab_account.png", nil];
    NSArray *tabbarIconPressedArray = [[NSArray alloc] initWithObjects:@"icn_home_note-pressed@2x.png", @"icn_tab_note-pressed@2x.png", @"icn_tab_account-pressed@2x.png", nil];
    [self.tabBarController.tabBar setTintColor:[UIColor whiteColor]];
    
    NSInteger index = 0;
    
    for(UITabBarItem * tabBarItem in self.tabBarController.tabBar.items){
        tabBarItem.title = @"";
        tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        UIImage *unselectedImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@", [tabbarIconArray objectAtIndex:index]]];
        UIImage *selectedImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@", [tabbarIconPressedArray objectAtIndex:index]]];
        
        [tabBarItem setImage: [unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        [tabBarItem setSelectedImage: selectedImage];
        index++;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
