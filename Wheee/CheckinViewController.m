//
//  CheckinViewController.m
//  Wheee
//
//  Created by Hungchi on 1/29/16.
//  Copyright © 2016 Hungchi. All rights reserved.
//

#import "CheckinViewController.h"
#import "ProductDetailViewController.h"
#import "PicgoService.h"
#import "SchoolProductListTableViewCell.h"

@interface CheckinViewController ()
- (IBAction)cameraAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *cameraBtn;

@property UIImagePickerController *pickerController;
@property NSInteger schoolorProduct;//0:school;1:product
@property NSInteger productIndex;
@property NSInteger schoolIndex;
@property UIImageView *selectedSchoolImage;
@property UIImageView *selectedProductImage;
@property NSString *selectedSchoolName;
@property NSString *selectedProductName;
@property NSMutableArray *listofSchools;
@property NSMutableArray *listofProducts;
@property NSMutableArray *listofProductsTags;
@property NSMutableArray *photoArray;
@property UITextField *messageField;
@property NSInteger didSelectSchoolIndex;
@property NSInteger didSelectProductIndex;

@end

@implementation CheckinViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%f,%f",self.view.frame.size.width,self.view.frame.size.height);
    NSLog(@"_theSchool:::%@",_theSchool);
    // Do any additional setup after loading the view.
    self.textView.tag = 0;
    self.productIndex = -1;
    self.schoolIndex = -1;
    self.saveBtn.tag = -1;
    self.didSelectSchoolIndex = -1;
    self.didSelectProductIndex = -1;
    self.searchProductTextField.hidden = YES;
    self.coverView.hidden = YES;
    self.photoArray = [[NSMutableArray alloc] init];
    
    self.listofSchools = [[NSMutableArray alloc] initWithObjects:@"台灣科技大學",@"雲林科技大學",@"長庚大學", nil];
    self.listofProducts = [[NSMutableArray alloc] initWithObjects:@"Crimson 87",@"另一個宇宙",@"The Little Music Paver",@"WEILI",@"小明與小美",@"爆炸頭阿蟹", nil];
    self.listofProductsTags = [[NSMutableArray alloc] initWithObjects:@"動畫",@"動畫／兒童",@"產品／辦公室",@"APP",@"產品／兒童",@"APP／辦公室／產品", nil];

    if (_theSchool != NULL) {
        self.productName.userInteractionEnabled = NO;
        [self.schoolName setUserInteractionEnabled:YES];
        self.chooseSchool.enabled = NO;
        UIImage *btnImage = [UIImage imageNamed:@"btn-school-p@2x.png"];
        [self.chooseSchool setImage:btnImage forState:UIControlStateNormal];
        self.schoolName.text = _theSchool;
        self.productName.text = _theProduct;
        self.schoolName.textColor = [UIColor colorWithRed:0.13 green:0.24 blue:0.4 alpha:1.0];
        self.productName.textColor = [UIColor colorWithRed:0.13 green:0.24 blue:0.4 alpha:1.0];
    }
    else{
        self.productName.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapLabelWithGesture:)];
        [self.productName addGestureRecognizer:tapGesture];
        
        UITapGestureRecognizer *schoolNameTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(schoolNameTapDetected)];
        schoolNameTap.numberOfTapsRequired = 1;
        [self.schoolName setUserInteractionEnabled:YES];
        [self.schoolName addGestureRecognizer:schoolNameTap];
        
        [self.view addSubview:self.scrollView];
        [self setupScrollView:self.scrollView];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFetchPhotosByNameSuccessNotification:) name:FetchPhotosByNameSuccessNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [[PicgoService sharedInstance] fetchPhotosByName:@"HC"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)onFetchPhotosByNameSuccessNotification:(NSNotification*) notify {
//    NSLog(@"onFetchPhotosByNameSuccessNotification:%@", notify.object);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    if ([self.textView isFirstResponder] && [touch view] != self.textView) {
        [self.textView resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    // done button was pressed - dismiss keyboard
    if (textField == self.searchProductTextField) {
        [self goSearch];
        NSLog(@"self.searchProductTextField");
    }
    else
        NSLog(@"message textField");
    
    return YES;
}

- (void) hideKeyboard {
    [self.productName resignFirstResponder];
    [self.messageField resignFirstResponder];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (self.schoolorProduct) {
        case 0:
            return [self.listofSchools count];
        default:
            return [self.listofProducts count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *schoolProductListIdentifier = @"SchoolProductListCell";
    
    SchoolProductListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:schoolProductListIdentifier];
    
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            [view removeFromSuperview];
        }
    }
    if (cell == nil) {
        cell = [[SchoolProductListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:schoolProductListIdentifier];
    }
    if (self.schoolorProduct == 0) {
        cell.schoolList.text = [self.listofSchools objectAtIndex:indexPath.row];
        cell.schoolList.hidden = NO;
        cell.productList.hidden = YES;
        cell.productTag.hidden = YES;
        self.selectedProductImage.hidden = YES;
        if ([cell.schoolList.text isEqualToString:self.schoolName.text]) {
            self.didSelectSchoolIndex = indexPath.row;
            [self tableView:tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForItem:self.didSelectSchoolIndex inSection:0]];
            UIImage *didSelected = [UIImage imageNamed:@"icn-selected.png"];
            self.selectedSchoolImage =[[UIImageView alloc]initWithFrame:CGRectMake(cell.schoolList.frame.origin.x-34, 10, 24, 24)];
            self.selectedSchoolImage.image = didSelected;
            [cell addSubview:self.selectedSchoolImage];
            self.schoolIndex = indexPath.row;
        }
    }
    else if (self.schoolorProduct == 1) {
        cell.schoolList.hidden = YES;
        cell.productList.hidden = NO;
        cell.productTag.hidden = NO;
        self.selectedSchoolImage.hidden = YES;
        cell.productList.text = [self.listofProducts objectAtIndex:indexPath.row];
        cell.productTag.text = [self.listofProductsTags objectAtIndex:indexPath.row];
        if ([cell.productList.text isEqualToString:self.productName.text]) {
            self.didSelectProductIndex = indexPath.row;
            [self tableView:tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForItem:self.didSelectProductIndex inSection:0]];
            UIImage *didSelected = [UIImage imageNamed:@"icn-selected.png"];
            self.selectedProductImage =[[UIImageView alloc]initWithFrame:CGRectMake(8, 10, 24, 24)];
            self.selectedProductImage.image = didSelected;
            [cell addSubview:self.selectedProductImage];
            self.productIndex = indexPath.row;
        }
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SchoolProductListTableViewCell *cell = (SchoolProductListTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor colorWithRed:1 green:1 blue:0.98 alpha:0.75f];
    if (self.schoolorProduct==0) {
        if (self.schoolIndex < 0 || self.schoolIndex != indexPath.row) {
            if (indexPath.row != self.didSelectSchoolIndex) {
                [self tableView:tableView didDeselectRowAtIndexPath:[NSIndexPath indexPathForItem:self.didSelectSchoolIndex inSection:0]];
            }
            self.selectedSchoolName = [self.listofSchools objectAtIndex:indexPath.row];
            cell.schoolList.textColor = [UIColor colorWithRed:0.13 green:0.24 blue:0.4 alpha:1.0];
            cell.schoolList.font = [UIFont fontWithName:@"PingFang TC Bold" size:17.0f];
            
            UIImage *didSelected = [UIImage imageNamed:@"icn-selected.png"];
            self.selectedSchoolImage =[[UIImageView alloc]initWithFrame:CGRectMake(cell.schoolList.frame.origin.x-34, 10, 24, 24)];
            self.selectedSchoolImage.image = didSelected;
            [cell addSubview:self.selectedSchoolImage];
            self.schoolIndex = indexPath.row;
        }
        else{
            NSLog(@"重複選取");
        }
        self.didSelectSchoolIndex = indexPath.row;
    }
    else{
        if (self.productIndex < 0 || self.productIndex != indexPath.row) {
            if (indexPath.row != self.didSelectProductIndex) {
                [self tableView:tableView didDeselectRowAtIndexPath:[NSIndexPath indexPathForItem:self.didSelectProductIndex inSection:0]];
            }
            self.selectedProductName = [self.listofProducts objectAtIndex:indexPath.row];
            cell.productList.textColor = [UIColor colorWithRed:0.13 green:0.24 blue:0.4 alpha:1.0];
            cell.productList.font = [UIFont fontWithName:@"PingFang TC Bold" size:17.0f];
            cell.productList.frame = CGRectMake(42, cell.productList.frame.origin.y, cell.productList.intrinsicContentSize.width, 24);
            [cell.productList setTranslatesAutoresizingMaskIntoConstraints:YES];
            
            UIImage *didSelected = [UIImage imageNamed:@"icn-selected.png"];
            self.selectedProductImage =[[UIImageView alloc]initWithFrame:CGRectMake(8, 10, 24, 24)];
            self.selectedProductImage.image = didSelected;
            [cell addSubview:self.selectedProductImage];
            self.productIndex = indexPath.row;
        }
        else{
            NSLog(@"重複選取");
        }
        self.didSelectProductIndex = indexPath.row;
    }
}

-(void) tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    SchoolProductListTableViewCell *cell = (SchoolProductListTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor colorWithRed:1 green:1 blue:0.98 alpha:0.75f];
    
    if (self.schoolorProduct == 0) {
        cell.schoolList.textColor = [UIColor colorWithRed:0.392 green:0.392 blue:0.392 alpha:1.0];
        cell.schoolList.font = [UIFont fontWithName:@"PingFang TC" size:17.0f];
        
        self.selectedSchoolImage.tag = 100;
        [cell.contentView addSubview:self.selectedSchoolImage];
        [[cell.contentView viewWithTag:100] removeFromSuperview];
    }
    else {
        NSLog(@"%@:Deselect",cell.productList.text);
        cell.productList.textColor = [UIColor colorWithRed:0.392 green:0.392 blue:0.392 alpha:1.0];
        cell.productList.font = [UIFont fontWithName:@"PingFang TC" size:17.0f];
        cell.productList.frame = CGRectMake(8, 10, cell.productList.intrinsicContentSize.width, 24);
        
        self.selectedProductImage.tag = 111;
        [cell.contentView addSubview:self.selectedProductImage];
        [[cell.contentView viewWithTag:111] removeFromSuperview];
    }
}

- (void)setupScrollView:(UIScrollView*)myScrollView {
    
    for (UIView* view in [myScrollView subviews]) {
        if ([view isKindOfClass:[UIImageView class]]) {
            [view removeFromSuperview];
        }
    }
    for (int i=0; i<4; i++) {
        CGFloat sumHeight = 12;
        for (UIView* view in myScrollView.subviews) {
            sumHeight += (view.frame.size.height+12);
        }
        if (i<self.photoArray.count) {
            UIImage *photoImage  = [self.photoArray objectAtIndex:i];

            CGFloat scale = (self.view.frame.size.width - 24)/photoImage.size.width;
            CGFloat straightResize = photoImage.size.width/photoImage.size.height;
            CGFloat XY = photoImage.size.width/photoImage.size.height;
            
            if (XY < 1) {
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake( (self.view.frame.size.width-photoImage.size.width*straightResize*scale)/2, sumHeight, photoImage.size.width*straightResize*scale, photoImage.size.height*straightResize*scale)];
                imageView.contentMode=UIViewContentModeScaleToFill;
                [imageView setImage:photoImage];
                imageView.tag=i+1;
                //Delete photo Button
                UIButton *deletePhotoBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
                UIImage *btnImage = [UIImage imageNamed:@"btn-delete-pic@2x.png"];
                [deletePhotoBtn setImage:btnImage forState:UIControlStateNormal];
                UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]
                                                         initWithTarget:self action:@selector(deletePhoto:)];
                [tapRecognizer setNumberOfTouchesRequired:1];
                [deletePhotoBtn setUserInteractionEnabled:YES];
                [deletePhotoBtn addGestureRecognizer:tapRecognizer];
                deletePhotoBtn.tag = imageView.tag-1;
                [imageView setUserInteractionEnabled:YES];
                [imageView addSubview:deletePhotoBtn];
                [myScrollView addSubview:imageView];
            }
            else{
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake( 12, sumHeight, photoImage.size.width*scale, photoImage.size.height*scale)];
                imageView.contentMode=UIViewContentModeScaleToFill;
                [imageView setImage:photoImage];
                imageView.tag=i+1;

                //Delete photo Button
                UIButton *deletePhotoBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
                UIImage *btnImage = [UIImage imageNamed:@"btn-delete-pic@2x.png"];
                [deletePhotoBtn setImage:btnImage forState:UIControlStateNormal];
                UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]
                                                         initWithTarget:self action:@selector(deletePhoto:)];
                [tapRecognizer setNumberOfTouchesRequired:1];
                [deletePhotoBtn setUserInteractionEnabled:YES];
                [deletePhotoBtn addGestureRecognizer:tapRecognizer];
                deletePhotoBtn.tag = imageView.tag-1;
                [imageView setUserInteractionEnabled:YES];
                [imageView addSubview:deletePhotoBtn];
                [myScrollView addSubview:imageView];
            }
        }
        else{
            UIImage *image = [UIImage imageNamed:@"addImageSquare.png"];
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake( 12, sumHeight, self.view.frame.size.width-24, self.view.frame.size.width-24)];
            imageView.contentMode=UIViewContentModeScaleToFill;
            [imageView setImage:image];
            //Tapped,then open Camera
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
            singleTap.numberOfTapsRequired = 1;
            [imageView setUserInteractionEnabled:YES];
            [imageView addGestureRecognizer:singleTap];
            imageView.tag=i+1;
            [myScrollView addSubview:imageView];
        }
    }
    CGFloat scrollViewHeight = 0.0f;
    for (UIView* view in myScrollView.subviews)
    {
        scrollViewHeight += (view.frame.size.height+12);
    }
    [myScrollView setContentSize:(CGSizeMake(self.view.frame.size.width, scrollViewHeight))];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveBtn:(id)sender {
    if (self.saveBtn.tag == 0) {
        UIImage *btnImage = [UIImage imageNamed:@"btn-school-p@2x.png"];
        [self.chooseSchool setImage:btnImage forState:UIControlStateNormal];
        self.schoolName.text = self.selectedSchoolName;
        self.schoolName.textColor = [UIColor colorWithRed:0.13 green:0.24 blue:0.4 alpha:1.0];
        self.coverView.hidden = YES;
        self.scrollView.hidden = NO;
        [self.tableView reloadData];
        self.saveBtn.tag = -1;
    }
    else if (self.saveBtn.tag == 1){
        self.productName.text = self.selectedProductName;
        self.productName.textColor = [UIColor colorWithRed:0.13 green:0.24 blue:0.4 alpha:1.0];
        self.coverView.hidden = YES;
        self.scrollView.hidden = NO;
        [self.tableView reloadData];
        [self.searchProductTextField resignFirstResponder];
        self.searchProductTextField.hidden = YES;
        self.saveBtn.tag = -1;
    }
    else if ([self.productName.text isEqualToString:@"請輸入作品名稱"]||[self.schoolName.text isEqualToString:@"學校"]) {
        UIAlertView *theAlert = [[UIAlertView alloc] initWithTitle:@""
                                                           message:@"尚未選取學校名稱與做品名稱"
                                                          delegate:self
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [theAlert show];

    }
    else {
//        UIAlertView *theAlert = [[UIAlertView alloc] initWithTitle:@"SAVE"
//                                                           message:@""
//                                                          delegate:self
//                                                 cancelButtonTitle:@"OK"
//                                                 otherButtonTitles:nil];
//        [theAlert show];
        ProductDetailViewController *productDetailViewController = [[ProductDetailViewController alloc] init];
        productDetailViewController.data = @"save"; // Set the exposed property
        productDetailViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:productDetailViewController animated:YES completion:nil];
        [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
        
    }
}

- (IBAction)cancelBtn:(id)sender {
    if (self.coverView.hidden == NO) {
        self.coverView.hidden = YES;
        self.scrollView.hidden = NO;
        self.searchProductTextField.hidden = YES;
    }
    else{
//        [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
        [self passDataForward];
    }
}

- (void)passDataForward{
    ProductDetailViewController *productDetailViewController = [[ProductDetailViewController alloc] init];
    productDetailViewController.data = @"cancel"; // Set the exposed property
    productDetailViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:productDetailViewController animated:NO completion:nil];
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)chooseSchool:(id)sender {
    self.scrollView.hidden = YES;
    self.coverView.hidden = NO;
    self.schoolorProduct = 0;
    self.saveBtn.tag = 0;
    self.saveBtn.titleLabel.text = @"確認";
    [self.tableView reloadData];
}

- (void)didTapLabelWithGesture:(UITapGestureRecognizer *)tapGesture {
    self.coverView.hidden = NO;
    self.scrollView.hidden = YES;
    self.schoolorProduct = 1;
    self.saveBtn.tag = 1;
    self.saveBtn.titleLabel.text = @"確認";
    self.searchProductTextField.hidden = NO;
    [self.searchProductTextField becomeFirstResponder];
    [self.tableView reloadData];
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView {
    if(textView.tag == 0) {
        textView.text = @"123456";
        textView.textColor = [UIColor blackColor];
        textView.tag = 1;
    }
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if([textView.text length] == 0)
    {
        textView.text = @"Foobar placeholder";
        textView.textColor = [UIColor lightGrayColor];
        textView.tag = 0;
    }
}

-(void)tapDetected{
    NSLog(@"single Tap on imageview");
    [self cameraAction:self];
}

-(void) deletePhoto:(id)sender {
    UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer *)sender;
//    NSLog (@"%ld",tapRecognizer.view.tag);
    [self.photoArray removeObjectAtIndex:tapRecognizer.view.tag];
    [self setupScrollView:self.scrollView];
}

-(void) schoolNameTapDetected {
    [self chooseSchool:self];
}

-(void) goSearch {
    NSLog(@"%@",self.searchProductTextField.text);
}

- (void) openCameraWithoutSegue
{
    DBCameraViewController *cameraController = [DBCameraViewController initWithDelegate:self];
    [cameraController setUseCameraSegue:NO];
//    [cameraController setForceQuadCrop:YES];
    
    DBCameraContainerViewController *container = [[DBCameraContainerViewController alloc] initWithDelegate:self];
    [container setCameraViewController:cameraController];
    [container setFullScreenMode];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:container];
    [nav setNavigationBarHidden:YES];
    [self presentViewController:nav animated:YES completion:nil];
}

- (void) openCameraWithoutContainer
{
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:[DBCameraViewController initWithDelegate:self]];
    [nav setNavigationBarHidden:YES];
    [self presentViewController:nav animated:YES completion:nil];
}

- (IBAction)cameraAction:(id)sender {
    /*
     BOOL isCameraAvailable = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
     BOOL isPhotoLibraryAvailable = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary];
     
     UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
     
     [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
     
     if (isCameraAvailable) {
     [alertController addAction:[UIAlertAction actionWithTitle:@"使用相機拍攝" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
     
     DBCameraContainerViewController *cameraContainer = [[DBCameraContainerViewController alloc] initWithDelegate:self];
     [cameraContainer setFullScreenMode];
     
     UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cameraContainer];
     [nav setNavigationBarHidden:YES];
     [self presentViewController:nav animated:YES completion:nil];
     
     //            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
     //            picker.delegate = self;
     //            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
     //            picker.allowsEditing = YES;
     //            if([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0) {
     //                picker.modalPresentationStyle = UIModalPresentationOverCurrentContext;
     //            }
     //            picker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
     //            self.pickerController = picker;
     //            [self presentViewController:self.pickerController animated:YES completion:nil];
     }]];
     }
     
     if (isPhotoLibraryAvailable) {
     [alertController addAction:[UIAlertAction actionWithTitle:@"從相簿中選取" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
     QBImagePickerController *imagePickerController = [QBImagePickerController new];
     imagePickerController.delegate = self;
     imagePickerController.allowsMultipleSelection = YES;
     imagePickerController.maximumNumberOfSelection = 6;
     imagePickerController.showsNumberOfSelectedAssets = YES;
     
     [self presentViewController:imagePickerController animated:YES completion:NULL];
     }]];
     }*/
    
    DBCameraContainerViewController *cameraContainer = [[DBCameraContainerViewController alloc] initWithDelegate:self];
    [cameraContainer setFullScreenMode];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cameraContainer];
    [nav setNavigationBarHidden:YES];
    [self presentViewController:nav animated:YES completion:nil];
}

//Use your captured image
#pragma mark - DBCameraViewControllerDelegate

- (void) camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata{
    [self.photoArray addObject:image];
    [self setupScrollView:self.scrollView];
    [cameraViewController restoreFullScreenMode];
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void) dismissCamera:(id)cameraViewController{
    [self dismissViewControllerAnimated:YES completion:nil];
    [cameraViewController restoreFullScreenMode];
}

/*
- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didFinishPickingAssets:(NSArray *)assets {
    for (PHAsset *asset in assets) {
        // Do something with the asset
        NSLog(@"didFinishPickingAssets");
        PHImageManager *manager = [PHImageManager defaultManager];
        PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
        options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
        options.synchronous = YES;
        options.networkAccessAllowed = YES;
        options.progressHandler = ^(double progress, NSError *error, BOOL *stop, NSDictionary *info) {
            NSLog(@"%f", progress);
        };
        
        [manager requestImageDataForAsset:asset
                                  options:options
                            resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info)
         {
             UIImage *image = [UIImage imageWithData:imageData];
             CGFloat scale = (self.view.frame.size.width - 24)/image.size.width;
             self.selectedImageView.image = image;
             self.selectedImageView.frame = CGRectMake(12, self.messageTextField.frame.origin.y+self.messageTextField.intrinsicContentSize.height+10, image.size.width*scale, image.size.height*scale);
             [self.selectedImageView setTranslatesAutoresizingMaskIntoConstraints:YES];
             self.deleteImageBtn.hidden = NO;
             [self.cameraBtn setBackgroundImage:[UIImage imageNamed:@"btn-camera-p@2x.png"]
                                       forState:UIControlStateNormal];
             
         }];
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController {
    [self dismissViewControllerAnimated:YES completion:NULL];
}
 
#pragma mark - UIImagePickerController Delegate Methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    self.deleteImageBtn.hidden = NO;
    
    [self.cameraBtn setBackgroundImage:[UIImage imageNamed:@"btn-camera-p@2x.png"]
                              forState:UIControlStateNormal];
    [self.cameraBtn setImageEdgeInsets:UIEdgeInsetsMake(5.0, 5.0, 5.0, 5.0)];
    self.selectedImageView.image = info[UIImagePickerControllerEditedImage];
    self.selectedImageView.frame = CGRectMake(12, self.messageTextField.frame.origin.y+self.messageTextField.intrinsicContentSize.height+10, self.view.frame.size.width-24, self.view.frame.size.width-24);
    [self.selectedImageView setTranslatesAutoresizingMaskIntoConstraints:YES];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
*/

@end
