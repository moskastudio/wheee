//
//  CheckinViewController.h
//  Wheee
//
//  Created by Hungchi on 1/29/16.
//  Copyright © 2016 Hungchi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QBImagePickerController.h"
#import "DBCameraViewController.h"
#import "DBCameraContainerViewController.h"
#import "ProductDetailViewController.h"

@interface CheckinViewController : UIViewController
<QBImagePickerControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UISearchBarDelegate,UISearchControllerDelegate,DBCameraViewControllerDelegate,UITextFieldDelegate>

- (IBAction)saveBtn:(id)sender;
- (IBAction)cancelBtn:(id)sender;
- (IBAction)chooseSchool:(id)sender;
- (void)setupScrollView:(UIScrollView*)myScrollView ;

@property (strong, nonatomic) IBOutlet UITextView *textView;
@property(nonatomic) CGSize contentSize;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UITextField *searchProductTextField;
@property (strong, nonatomic) IBOutlet UIButton *saveBtn;
@property (strong, nonatomic) IBOutlet UIButton *chooseSchool;
@property (strong, nonatomic) IBOutlet UIView *coverView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *schoolName;
@property (strong, nonatomic) IBOutlet UILabel *productName;

@property (nonatomic, retain) NSString *theSchool;
@property (nonatomic, retain) NSString *theProduct;
@end
